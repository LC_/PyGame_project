import pygame, time, os
from pygame.locals import *

class Menu:	
	os.environ['SDL_VIDEO_CENTERED'] = '1'
	pygame.init()


	# title_name = ""
	# title_font = ''
	# title_size = 0
	# # title_position = (self.weight/2,self.height/5)
	# # weigth e height anda nao foram declarados, e pra declarar..
	# title_color = (0,0,0)
	list_options = ["Start Game","Records","Settings","Quit"]
	options_font = 'fonts/munro.ttf'
	options_size = 40
	options_position_X = 0
	options_position_Y = 200
	options_color = (255,255,255)

	select = list_options[0]
	menu_stage = 0
	active_selection_square = True
	active_selection_name = True
	selection_square_paint = True
	selection_square_color = (255,255,0)
	selection_name_color = (255,255,0)

	space_between_options = 70
	#display = pygame.Surface

	def __init__(self, display_width, display_height, background_color, display_name):
		pygame.display.set_caption(display_name)			
		self.display_width = display_width
		self.display_height = display_height
		self.background_color = background_color
		self.display = pygame.display.set_mode((self.display_width,self.display_height), RESIZABLE)
		self.display.fill(self.background_color) #color background

	def title(self, title, font, size, position, color):
		largeText = pygame.font.Font(font, size)
		TextSurf = largeText.render(title, True, color)
		TextRect = TextSurf.get_rect()
		TextRect.center = position
		self.display.blit(TextSurf, TextRect)

	def options(self, list_options, font, size, positon, color):
    	# funcao options e em funcao da funcao title
		self.list_options = list_options
		self.options_font = font
		self.options_size = size
		self.options_position = position
		self.options_color = color
	
	def draw(self, active_selection_square, 
				   active_selection_name,
		           selection_square_paint,
		           selection_square_color,
		           selection_name_color,
				   space_between_options):
		self.active_selection_square = active_selection_square
		self.active_selection_name = active_selection_name
		self.selection_name_color = selection_name_color
		self.selection_square_color = selection_square_color
		self.selection_square_paint = selection_square_paint
		self.space_between_options = space_between_options
	
	def selection(self, num): 
		self.menu_stage += num
		for i in range(0,len(self.list_options)):
			if self.menu_stage%4 == i: 
				self.select = self.list_options[i]

	def draw_selection(self):
		for i in range(0,len(self.list_options)):
			if self.select == self.list_options[i]:
				self.title(self.list_options[i],self.options_font,self.options_size,(self.display_width/2,self.display_height/2.2+self.space_between_options*i), self.selection_name_color)
			else:
				self.title(self.list_options[i],self.options_font,self.options_size,(self.display_width/2,self.display_height/2.2+self.space_between_options*i), self.options_color)
	
	def run(self):
		loop_menu = True
		while loop_menu:
			for event in pygame.event.get():
				if event.type == QUIT:
					loop_menu = False
				
				if event.type==pygame.KEYDOWN:

					if event.key==pygame.K_DOWN:				
						self.selection(1)
					
					if event.key==pygame.K_UP:
						self.selection(-1)	

			self.draw_selection()

			pygame.display.update()


optionsList = ["Start Game","Records","Settings","Quit"]

x=500
y=500

menu = Menu(x,y, (0,0,0), "Menu Game")

menu.title("Snake", 'fonts/8bit.ttf', 65, (x/2,y/5), (255,255,255))
# menu.options(optionsList,'fonts.munro.ttf',40,)
# menu.options(options,'fonts/munro', 40,)
menu.run()