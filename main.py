import pygame, os
import time
import random
 
def message_display(text, font, size, X, Y, color):
    largeText = pygame.font.Font(font, size)
    TextSurf = largeText.render(text, True, color)
    TextRect = TextSurf.get_rect()
    TextRect.center = (X,Y)
    gameDisplay.blit(TextSurf, TextRect)

def selection(semenu_stage, num): 
    menu_stage += num
    for i in range(0,4):
        if menu_stage%4 == i: 
            select = menu_options[i]

def run_menu():

    intro = True

    select = menu_options[0]
    menu_stage = 0    

    pygame.key.set_repeat(200,70)

    while intro:
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                intro = False
                            
            if event.type==pygame.KEYDOWN:
                if event.key==pygame.K_DOWN:
                    # selection(1)                        
                    menu_stage += 1
                    for i in range(0,len(menu_options)):
                        if menu_stage%len(menu_options) == i:
                           select = menu_options[i]
                                    
                if event.key==pygame.K_UP:
                    menu_stage += -1
                    
                    for i in range(0,len(menu_options)):
                        if menu_stage%len(menu_options) == i:
                           select = menu_options[i]

                if event.key==pygame.K_RETURN:
                    if select == menu_options[3]: 
                        intro = False


        gameDisplay.fill(black)
        message_display(menu_title, 'fonts/8bit.ttf', 65, display_width/2, display_height/5, white)

        if select == menu_options[0]:
            pygame.draw.rect(gameDisplay, white, (150, 203, 200, size_options))
            message_display(menu_options[0], 'fonts/munro.ttf', size_options, display_width/2, 225, black)
        else:
            message_display(menu_options[0], 'fonts/munro.ttf', size_options, display_width/2, 225, white)

        if select == menu_options[1]:
            pygame.draw.rect(gameDisplay, white, (150, 253, 200, size_options))            
            message_display(menu_options[1], 'fonts/munro.ttf', size_options, display_width/2, 275, black)
        else:
            message_display(menu_options[1], 'fonts/munro.ttf', size_options, display_width/2, 275, white)

        if select == menu_options[2]:
            pygame.draw.rect(gameDisplay, white, (150, 303, 200, size_options))            
            message_display(menu_options[2], 'fonts/munro.ttf', size_options, display_width/2, 325, black)
        else:
            message_display(menu_options[2], 'fonts/munro.ttf', size_options, display_width/2, 325, white)

        if select == menu_options[3]:
            pygame.draw.rect(gameDisplay, white, (150, 353, 200, size_options))            
            message_display(menu_options[3], 'fonts/munro.ttf', size_options, display_width/2, 375, black)
        else:
            message_display(menu_options[3], 'fonts/munro.ttf', size_options, display_width/2, 375, white)

        pygame.display.update()           

pygame.init()

os.environ['SDL_VIDEO_CENTERED'] = '1' 

display_width = 500
display_height = 500
 
black = (0,0,0)
white = (255,255,255)
red = (255,0,0)
green = (0,200,0)
yellow=(255, 255, 0)

# tentar proporcoes de variaveis com apenas o menu de cores
size_title = 65
size_options = 40
menu_title = "Snake"
menu_options = ["Start Game","Records","Settings","Quit"]

pygame.display.set_caption('Snake Game')
gameDisplay = pygame.display.set_mode((display_width,display_height))

run_menu()